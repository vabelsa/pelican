Pelican - Creación de contenido
###############################


:date: 2013-04-11 10:20
:tags: pelican, html, marcas
:category: clase
:slug: Pelican
:author: Vanessa
:summary: Short version for index and feeds


Crear contenido con **Pelican** es lo más sencillo que uno pueda imaginarse. Por 
ejemplo, crear este articulo, se puede hacer del siguiente modo. Abrimos un 
editor de textos cualquiera (por ejemplo vi o gedit) y creamos un archivo nuevo 
llamado Pelican, con la extensión *.md* si queremos emplear Markdown o *.rst* si 
queremos emplear reStructuredText.
